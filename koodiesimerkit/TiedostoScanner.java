import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class TiedostoScanner {

	/**
	 * Tiedostoja voi lukea my�s scannerilla:
	 * @param args
	 * @throws FileNotFoundException 
	 */
	public static void main(String[] args) throws FileNotFoundException {
		
		String tiedostonimi = args[0]; //otetaan tiedostonimi komentorivilt�
		File tiedosto = new File(tiedostonimi); //File -olio viittaa t�h�n kyseiseen tiedostoon
		
		Scanner s = new Scanner(tiedosto); //Scanner asetetaan lukemaan tiedostoa, eik� "System.in" -sy�tevirtaa.
		
		while(s.hasNextLine()) { //jatketaan niin kauan kuin scanner ilmoittaa, ett� rivej� on viel� j�ljell�.
			String rivi = s.nextLine();
			System.out.println(rivi);
		}
		s.close();
	}
}
