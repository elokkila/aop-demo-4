import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;

public class Sy�tevirrat {

	/**
	 * Sy�tevirroilla (inputstream) voidaan lukea tiedostoja juuri niin paljon kerrallaan kuin tarvitsee.
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		
		File f = new File(args[0]);
		
		FileInputStream fis = new FileInputStream(f);
			
		byte[] puskuri=new byte[100]; //100 tavun puskuri
		int luettu=0;
		
		 //yritet��n lukea puskuriin 100 tavua. Metodi tallentaa tiedot puskuriin ja palauttaa todellisuudessa luetun m��r�n.
		//jatketaan while-silmukkaa, kunnes metodi palauttaa -1 (virta p��ttyi eik� luettavaa en�� ole)
		while((luettu=fis.read(puskuri))!=-1){
			System.out.println("Luettiin " + luettu + " tavua.");
			//muutetaan puskurin sis�lt� merkkijonoksi, joka sis�lt�� vain tavujen numeeriset arvot.
			String tavut = Arrays.toString(puskuri);
			System.out.println(tavut);
			
			//T�ss� voisi my�s kokeilla tavujen muuttamista merkkijonoksi, esim new String(puskuri); mutta se ei v�ltt�m�tt� onnistu
			//koska yht� merkki� saatetaan esitt�� useammalla tavulla merkist�koodauksesta riippuen ja puskuri saattaa katketa keskelt�
			//merkki�.
			//sy�tevirtoja k�ytet��n usein muiden tiedostomuotojen kuin tekstien lukemiseen.
		}
		fis.close(); //suljetaan sy�tevirta.
	}
}
