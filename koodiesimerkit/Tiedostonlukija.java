import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

public class Tiedostonlukija {

	
	public static void main(String[] args) throws IOException {
		
		String tiedostonimi = args[0]; //otetaan tiedostonimi komentorivilt�
		File tiedosto = new File(tiedostonimi); //File -olio viittaa t�h�n kyseiseen tiedostoon
		Path polku = tiedosto.toPath(); 		//polku kertoo tiedoston tarkan sijainnin
		
		List<String> rivit = Files.readAllLines(polku); //Files -luokasta l�ytyy metodi joka lukee koko tiedoston kerralla List-olioon.
		//kannattaa huomata, ett� todella isoilla tiedostoilla edellinen ei k�y, jos tietokoneesi muisti ei riit�.
		
		//tulostetaan esimerkkin� kaikki tiedostossa olevat rivit
		for(int i = 0;i<rivit.size();i++) {
			String rivi=rivit.get(i);
			System.out.println(rivi);
		}

		
	}
}
