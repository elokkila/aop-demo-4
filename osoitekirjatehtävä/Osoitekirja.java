import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Scanner;

public class Osoitekirja {


	public static void main(String[] args) {
		//TODO: T�ss� kannattaisi ehk� k�ytt�� listaa, jotta uusia rivej� on helpompi lis�t�.
		static String[][] osoitteet;
		
		/*
		 * Try-catch esitell��n viimeisell� (huomisella) luennolla.
		 * Try-catch-kontrollirakenteen idea on siis yritt�� tehd� jotakin, joka
		 * saattaa heitt�� poikkeuksen. Jos poikkeus heitet��n yrityksen
		 * aikana, otetaan se kiinni ja reagoidaan
		 * tilanteeseen, jotta ohjelman suoritus voi
		 * jatkua normaalisti.
		 */
	`	try { //yritet��n tehd� jotain
			osoitteet = lueOsoitteet();
		} catch (IOException e) { //tapahtui virhe, joka otetaan "kiinni"
			//mit� tehd��n kun virhe tapahtui:
			System.out.println("Osoitetiedostoa ei l�ytynyt!");
			osoitteet=new String[0][3];
			e.printStackTrace();
		}
		
		Scanner s = new Scanner(System.in);
		int valinta=-1;
		do {
			tulostaOhje();
			//TODO: parseInt try-catchiin?
			valinta = Integer.parseInt(s.nextLine());
			switch (valinta) {
			case 1:
				tulostaOsoitteet();
				break;
			case 2:
			
			default:
				break;
			}
			
		}while(valinta!=4);
		tallennaOsoitteet();
		System.out.println("Osoitteet tallennettu, poistutaan.");
		s.close();
	}



	public static String[][] lueOsoitteet() throws IOException {
		//TODO: pit�isik� t�m� saada k�ytt�j�lt�?
		File f = new File("osoitteet.csv");
		System.out.println(f.getAbsolutePath());
//		Path p = Paths.get(); // saadaan suoraa polku joka annetaan parametriksi readAllLines -metodille
		List<String> rivit = Files.readAllLines(f.toPath());
		osoitteet = new String[rivit.size()][3];

		for (int i = 0; i < rivit.size(); i++) {
			String[] arvot = rivit.get(i).split(",");
			osoitteet[i] = arvot; // tallennetaan yksiuloitteinen taulukko yhdeksi riviksi kaksiulotteiseen
									// taulukkoon
		}
		return osoitteet;
	}
	
	/**
	*Luetaan k�ytt�j�lt� uusi osoite, joka lis�t��n osoitekirjaan
	* TODO: params?
	*/
	public static String[] lueUusiOsoite() {
		return null;
		
	}
	/**
	 * Metodi tallentaa osoitekirjan, jotta ohjelman suorituksen aikana lis�tyt uudet tiedot ovat k�yt�ss� my�s ohjelman seuraavalla suorituskerralla
	 */
	public static void tallennaOsoitteet() {
		
		
	}
	public static void tulostaOsoitteet(String[][] osoitteet) {
		System.out.println("\nOsoitelista:");
		System.out.println("Nimi\tOsoite\tPuhelinnumero");
		for(String[] rivi : osoitteet) {
			System.out.println(rivi[0]+"\t"+rivi[1]+"\t"+rivi[2]);
		}
		System.out.println("\n");
	}
	
	private static void tulostaOhje() {
		System.out.println("Sy�t� toiminto:");
		System.out.println("1. Tulosta osoitekirja");
		System.out.println("2. Lis�� osoite");
		System.out.println("3. poista osoite");
		System.out.println("4. tallenna ja lopeta");
		
	}
}
